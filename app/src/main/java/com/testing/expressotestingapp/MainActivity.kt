package com.testing.expressotestingapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btMainActivity.setOnClickListener{
            startActivity(Intent(this@MainActivity, SecondaryActivity::class.java))
        }
    }
}