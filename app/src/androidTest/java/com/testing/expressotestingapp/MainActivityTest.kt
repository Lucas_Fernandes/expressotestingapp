package com.testing.expressotestingapp

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class MainActivityTest {

    @get:Rule
    val activityScenario = ActivityScenarioRule(MainActivity::class.java)


    @Test
    fun test_is_activity_in_view() {
        onView(withId(R.id.main)).check(matches(isDisplayed()))
    }

    @Test
    fun test_visibility_title_nextButton() {
        onView(withId(R.id.tvMainActivity)).check(matches(isDisplayed()))

        onView(withId(R.id.btMainActivity))
            .check(matches(withEffectiveVisibility(Visibility.VISIBLE))) // CHECKING VISIBILITY IN A DIFFERENT WAY
    }

    @Test
    fun test_is_title_text_displayed() {
        onView(withId(R.id.tvMainActivity))
            .check(matches(withText(R.string.text_main_activity)))
    }


    @Test
    fun test_nav_secondary_activity() { //PERFORMING BUTTON CLICK
        onView(withId(R.id.btMainActivity)).perform(click())

        onView(withId(R.id.secondary)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
    }

    @Test
    fun test_backpress_toMainActivity() {
        onView(withId(R.id.btMainActivity)).perform(click())

        onView(withId(R.id.secondary)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))

        onView(withId(R.id.btSecondaryActivity)).perform(click()) //RETURN METHOD 1

//        pressBack() //RETURN METHOD 2

        onView(withId(R.id.main)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
    }
}